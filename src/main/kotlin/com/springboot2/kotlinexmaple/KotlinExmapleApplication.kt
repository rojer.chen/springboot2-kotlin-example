package com.springboot2.kotlinexmaple

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinExmapleApplication

fun main(args: Array<String>) {
    runApplication<KotlinExmapleApplication>(*args)
}
