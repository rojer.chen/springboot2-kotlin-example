package com.springboot2.kotlinexmaple.controller

import com.springboot2.kotlinexmaple.common.ConstantData
import com.springboot2.kotlinexmaple.common.DEFAULT_NAME
import com.springboot2.kotlinexmaple.dto.Param1
import com.springboot2.kotlinexmaple.dto.Param2
import com.springboot2.kotlinexmaple.service.ExampleService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import kotlin.properties.Delegates

@RestController
class ExampleController {
//    注意lateinit 有坑，不能延迟初始化基本类型，比如int 和string 等。访问未初始化的 lateinit 属性会导致 UninitializedPropertyAccessException。
//    基本类型的延迟处理，使用 by delegate
//    private var mNumber: Int by Delegates.notNull<Int>()

    val logger: Logger = LoggerFactory.getLogger(javaClass)


    @Autowired
    lateinit var exmpaleService:ExampleService;

    @RequestMapping(path = arrayOf("/hello/{param}"),method = arrayOf(RequestMethod.GET))
    fun sayHello(@PathVariable("param") param:String):String{
        val res=exmpaleService.TestPlus(1,1)
        val result = param+res
        val target = listOf<Int>(1,2,3,4)
        val less =target.asSequence().filter { it <3 }.toList()
        val contain=target.contains(2)
        val contain2=2 in target
        target.forEach(label@{println(it)})
        logger.info("container is ${contain}")
        logger.info("container2 is ${contain2}")

        return result
    }

    @RequestMapping(path = arrayOf("/param"),method = arrayOf(RequestMethod.GET))
    fun sayHello():Param1{
//        注意两种不同的定义和引用方式
        println(DEFAULT_NAME)
//        注意不同的定义和引用方式
        println(ConstantData.URL)
        val p1 =Param1("cc",20)
        val pp1 =Param1.buildParam1()
        val p2=Param2()
        val pp2 = Param2("ccc",age=9)
        println("p2 name =${p2.name}")
        logger.info("log p1 ={}",p1)
        return p1
    }
}