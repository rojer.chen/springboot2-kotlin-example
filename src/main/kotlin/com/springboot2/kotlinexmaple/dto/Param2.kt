package com.springboot2.kotlinexmaple.dto

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.ALWAYS)
class Param2 {
    @JsonProperty("name")
    var name:String?
    @JsonProperty("age")
    var age:Int?
    init {
        this.name=""
        this.age=0
    }


    constructor(){
        this.name=""
        this.age=0
    }
    @JsonCreator
    constructor(name:String="",age:Int=0){
        this.name=""
        this.age=0
    }

}