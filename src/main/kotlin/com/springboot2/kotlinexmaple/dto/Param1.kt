package com.springboot2.kotlinexmaple.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
// json 的相关注解并不是必须的。
@JsonInclude(JsonInclude.Include.NON_EMPTY)
data class Param1(@JsonProperty("name")var name:String?="", @JsonProperty("age") var age :Int?=0):Serializable {


     companion object {
        fun buildParam1():Param1{
            return Param1("",0)
        }
    }
}